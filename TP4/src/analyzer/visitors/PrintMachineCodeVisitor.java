package analyzer.visitors;

import analyzer.ast.*;
import com.sun.org.apache.bcel.internal.generic.RETURN;
import javafx.util.Pair;

import java.io.PrintWriter;
import java.util.*;

public class PrintMachineCodeVisitor implements ParserVisitor {
    private PrintWriter m_writer = null;

    private int MAX_REGISTERS_COUNT = 256;

    private final ArrayList<String> RETURNS = new ArrayList<>();
    private final ArrayList<MachineCodeLine> CODE = new ArrayList<>();

    private final ArrayList<String> MODIFIED = new ArrayList<>();
    private final ArrayList<String> REGISTERS = new ArrayList<>();

    private final HashMap<String, String> OPERATIONS = new HashMap<>();

    public PrintMachineCodeVisitor(PrintWriter writer) {
        m_writer = writer;

        OPERATIONS.put("+", "ADD");
        OPERATIONS.put("-", "MIN");
        OPERATIONS.put("*", "MUL");
        OPERATIONS.put("/", "DIV");
    }

    @Override
    public Object visit(SimpleNode node, Object data) {
        return null;
    }

    @Override
    public Object visit(ASTProgram node, Object data) {
        node.childrenAccept(this, null);

        computeLifeVar();
        computeNextUse();

        printMachineCode();

        return null;
    }

    @Override
    public Object visit(ASTNumberRegister node, Object data) {
        MAX_REGISTERS_COUNT = ((ASTIntValue) node.jjtGetChild(0)).getValue();
        return null;
    }

    @Override
    public Object visit(ASTReturnStmt node, Object data) {
        for (int i = 0; i < node.jjtGetNumChildren(); i++) {
            RETURNS.add(((ASTIdentifier) node.jjtGetChild(i)).getValue());
        }
        return null;
    }

    @Override
    public Object visit(ASTBlock node, Object data) {
        node.childrenAccept(this, null);
        return null;
    }

    @Override
    public Object visit(ASTStmt node, Object data) {
        node.childrenAccept(this, null);
        return null;
    }

    @Override
    public Object visit(ASTAssignStmt node, Object data) {
        // TODO (ex1): Modify CODE to add the needed MachLine.
        // Here the type of Assignment is "assigned = left op right".
        // You can pass null as data to children.

        String ASSIGNED = (String) node.jjtGetChild(0).jjtAccept(this, null);
        String LEFT = (String) node.jjtGetChild(1).jjtAccept(this, null);
        String OPERATION = node.getOp();
        String RIGHT = (String) node.jjtGetChild(2).jjtAccept(this, null);

        MachineCodeLine MachLine = new MachineCodeLine(OPERATION, ASSIGNED, LEFT, RIGHT);

        CODE.add(MachLine);
        return null;
    }

    @Override
    public Object visit(ASTAssignUnaryStmt node, Object data) {
        // TODO (ex1): Modify CODE to add the needed MachLine.
        // Here the type of Assignment is "assigned = - right".
        // Suppose the left part to be the constant "#O".
        // You can pass null as data to children.

        String ASSIGNED = (String) node.jjtGetChild(0).jjtAccept(this, null);
        String LEFT = "#0";
        String OPERATION = "-";
        String RIGHT = (String) node.jjtGetChild(1).jjtAccept(this, null);

        MachineCodeLine MachLine = new MachineCodeLine(OPERATION, ASSIGNED, LEFT, RIGHT);

        CODE.add(MachLine);
        return null;
    }

    @Override
    public Object visit(ASTAssignDirectStmt node, Object data) {
        // TODO (ex1): Modify CODE to add the needed MachLine.
        // Here the type of Assignment is "assigned = right".
        // Suppose the left part to be the constant "#O".
        // You can pass null as data to children.

        String ASSIGNED = (String) node.jjtGetChild(0).jjtAccept(this, null);
        String LEFT = "#0";
        String OPERATION = "+";
        String RIGHT = (String) node.jjtGetChild(1).jjtAccept(this, null);

        MachineCodeLine MachLine = new MachineCodeLine(OPERATION, ASSIGNED, LEFT, RIGHT);

        CODE.add(MachLine);
        return null;
    }

    @Override
    public Object visit(ASTExpr node, Object data) {
        return node.jjtGetChild(0).jjtAccept(this, null);
    }

    @Override
    public Object visit(ASTIntValue node, Object data) {
        return "#" + node.getValue();
    }

    @Override
    public Object visit(ASTIdentifier node, Object data) {
        return node.getValue();
    }

    private void computeLifeVar() {
        // TODO (ex2): Implement life variables algorithm on the CODE array.

        for (MachineCodeLine node : CODE) {
            node.Life_IN = new HashSet<>();
            node.Life_OUT = new HashSet<>();
        }

        int lastIndex = CODE.size() - 1;
        CODE.get(lastIndex).Life_OUT = new HashSet<>(RETURNS);

        for (int i = CODE.size() - 1; i >= 0; i--) {
            if (i < (CODE.size() - 1))
                CODE.get(i).Life_OUT = new HashSet<>(CODE.get(i + 1).Life_IN);

            CODE.get(i).Life_IN = new HashSet<>(CODE.get(i).Life_OUT);
            CODE.get(i).Life_IN.removeAll(CODE.get(i).DEF);
            CODE.get(i).Life_IN.addAll(CODE.get(i).REF);
        }
    }

    private void computeNextUse() {
        // TODO (ex3): Implement next-use algorithm on the CODE array.
        for (MachineCodeLine node : CODE) {
            node.Next_IN = new NextUse();
            node.Next_OUT = new NextUse();
        }

        for (int i = CODE.size() - 1; i >= 0; i--) {
            if (i < (CODE.size() - 1))
                CODE.get(i).Next_OUT = (NextUse) CODE.get(i + 1).Next_IN.clone();

            for (Map.Entry<String, ArrayList<Integer>> mapEntry : CODE.get(i).Next_OUT.nextUse.entrySet()) {
                String v = mapEntry.getKey();
                ArrayList<Integer> n = (ArrayList<Integer>)mapEntry.getValue().clone();
                if (!CODE.get(i).DEF.contains(v))
                    CODE.get(i).Next_IN.nextUse.put(v,n);
            }
            for (String ref : CODE.get(i).REF)
                CODE.get(i).Next_IN.add(ref, i);
        }

    }

    /**
     * This function should generate the LD and ST when needed.
     */
    public String chooseRegister(String variable, HashSet<String> life, NextUse next, boolean loadIfNotFound) {
        // TODO (ex4): if variable is a constant (starts with '#'), return variable
        if(variable.startsWith("#"))
            return variable;

        // TODO (ex4): if REGISTERS contains variable, return "R" + index
        if(REGISTERS.contains(variable))
            return "R" + REGISTERS.indexOf(variable);

        // TODO (ex4): if REGISTERS size is not max (< MAX_REGISTERS_COUNT), add variable to REGISTERS and return "R" + index
        if(REGISTERS.size() < MAX_REGISTERS_COUNT){
            REGISTERS.add(variable);
            int idx = REGISTERS.indexOf(variable);
            if (loadIfNotFound)
                m_writer.println("LD R" + idx + ", " + variable);
            return "R" + idx;
        }

        // TODO (ex4): if REGISTERS has max size:
        // - put variable in space of an other variable which is not used anymore
        // *or*
        // - put variable in space of variable which as the largest next-use
        int largestNextUse = Integer.MIN_VALUE;
        String varToReplace = "";
        for (String currVar : REGISTERS) {
            if (!next.nextUse.containsKey(currVar)) {
                varToReplace = currVar;
                break;
            }
            int currNextUse = next.nextUse.get(currVar).get(0);
            if (largestNextUse < currNextUse) {
                varToReplace = currVar;
                largestNextUse = currNextUse;
            }
        }
        int toReplaceIndex = REGISTERS.indexOf(varToReplace);
        REGISTERS.set(toReplaceIndex, variable);

        Boolean needToStore = MODIFIED.contains(varToReplace) && life.contains(varToReplace);
        if (needToStore)
            m_writer.println("ST " + varToReplace + ", R" + toReplaceIndex);
        if (loadIfNotFound)
            m_writer.println("LD R" + toReplaceIndex + ", " + variable);

        return "R" + toReplaceIndex;
    }

    /**
     * Print the machine code in the output file
     */
    public void printMachineCode() {
        // TODO (ex4): Print the machine code in the output file.
        // You should change the code below.
        for (int i = 0; i < CODE.size(); i++) {
            MachineCodeLine currMachLine = CODE.get(i);
            m_writer.println("// Step " + i);
            String left = chooseRegister(currMachLine.LEFT, currMachLine.Life_IN, currMachLine.Next_IN, true);
            String right = chooseRegister(currMachLine.RIGHT, currMachLine.Life_IN, currMachLine.Next_IN, true);
            String assign = chooseRegister(currMachLine.ASSIGN, currMachLine.Life_OUT, currMachLine.Next_OUT, false);
            String operation = currMachLine.OPERATION;
            MODIFIED.add(currMachLine.ASSIGN);
            Boolean isLineUseless = Objects.equals(operation, OPERATIONS.get("+")) && Objects.equals(left, "#0") && Objects.equals(assign, right);
            if(!isLineUseless)
                m_writer.println(operation + " " + assign + ", " + left + ", " + right);
            m_writer.println(currMachLine);
        }

        for (String ret: RETURNS) {
            Boolean needToStore = REGISTERS.contains(ret) && MODIFIED.contains(ret);
            if (needToStore)
                m_writer.println("ST " + ret + ", R" + REGISTERS.indexOf(ret));
        }
    }

    /**
     * Order a set in alphabetic order
     *
     * @param set The set to order
     * @return The ordered list
     */
    public List<String> orderedSet(Set<String> set) {
        List<String> list = new ArrayList<>(set);
        Collections.sort(list);
        return list;
    }

    /**
     * A class to store and manage next uses.
     */
    private class NextUse {
        public HashMap<String, ArrayList<Integer>> nextUse = new HashMap<>();

        public NextUse() {}

        public NextUse(HashMap<String, ArrayList<Integer>> nextUse) {
            this.nextUse = nextUse;
        }

        public ArrayList<Integer> get(String s) {
            return nextUse.get(s);
        }

        public void add(String s, int i) {
            if (!nextUse.containsKey(s)) {
                nextUse.put(s, new ArrayList<>());
            }
            nextUse.get(s).add(i);
        }

        public String toString() {
            ArrayList<String> items = new ArrayList<>();
            for (String key : orderedSet(nextUse.keySet())) {
                Collections.sort(nextUse.get(key));
                items.add(String.format("%s:%s", key, nextUse.get(key)));
            }
            return String.join(", ", items);
        }

        @Override
        public Object clone() {
            return new NextUse((HashMap<String, ArrayList<Integer>>) nextUse.clone());
        }
    }

    /**
     * A struct to store the data of a machine code line.
     */
    private class MachineCodeLine {
        String OPERATION;
        String ASSIGN;
        String LEFT;
        String RIGHT;

        public HashSet<String> REF = new HashSet<>();
        public HashSet<String> DEF = new HashSet<>();

        public HashSet<String> Life_IN = new HashSet<>();
        public HashSet<String> Life_OUT = new HashSet<>();

        public NextUse Next_IN = new NextUse();
        public NextUse Next_OUT = new NextUse();

        public MachineCodeLine(String operation, String assign, String left, String right) {
            this.OPERATION = OPERATIONS.get(operation);
            this.ASSIGN = assign;
            this.LEFT = left;
            this.RIGHT = right;

            DEF.add(this.ASSIGN);
            if (this.LEFT.charAt(0) != '#')
                REF.add(this.LEFT);
            if (this.RIGHT.charAt(0) != '#')
                REF.add(this.RIGHT);
        }

        @Override
        public String toString() {
            String buffer = "";
            buffer += String.format("// Life_IN  : %s\n", Life_IN);
            buffer += String.format("// Life_OUT : %s\n", Life_OUT);
            buffer += String.format("// Next_IN  : %s\n", Next_IN);
            buffer += String.format("// Next_OUT : %s\n", Next_OUT);
            return buffer;
        }
    }
}
